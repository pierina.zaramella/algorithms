const names = [ "Luis LIV", "Luis II", "Carlos IX", "Carlos I","Carlos V",  "Juan X", "Juan XXLIV"]

const romanToNum = (value) => {
    switch (value) {
        case 'I': return 1;
        case 'V': return 5;
        case 'X': return 10;
        case 'L': return 50;
        default: return -1;
     }         
}

const arr = names.map((fullName, index) => {
    const [name, romanNum] = fullName.split(" ")
    console.log(name, romanNum)
    const arrRoman = romanNum.split("")
    let sum = romanToNum(arrRoman[0]);
    let pre, curr;
    for(let i=1; i < arrRoman.length; i++){
        curr = romanToNum(arrRoman[i]);
        pre = romanToNum(arrRoman[i-1]);
        if(curr <= pre){
            sum += curr;
        } else {
            sum = sum - pre*2 + curr;
        }
    }
    
    return  {fullName, fullNameNum: name + " " + sum}
})

arr.sort((a,b) => a.fullNameNum > b.fullNameNum)

let result = arr.map(a => a.fullName);
   
console.log(result)

