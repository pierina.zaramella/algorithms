const input = [
    "push 4",
    "pop",
    "push 3",
    "push 5",
    "push 2",
    "inc 3 1",
    "pop",
    "push 1",
    "inc 2 2",
    "push 4",
    "pop",
    "pop"
]

const createStack = () => {
    let stack = []
    const isEmpty = () => {
        return stack.length === 0
    }
    const lastItem = () => {
        return stack[0]
    }
    return {
        push: (item) => {
            stack.unshift(item)
            console.log("push ", stack)
            return item
        },
        pop: () => {
            stack = stack.slice(1)
            console.log("pop ", stack)
            return isEmpty() ? "EMPTY" : lastItem()
        },
        add: (i, addValue) => {
            const items = stack.slice(-i)
            items.reverse()
            items.map((val, ind) => {
                position = stack.length - (+ind + 1)
                stack[position] = +val + +addValue
            })
            console.log("add ", stack)
            return lastItem()
        }
    }
}

const opStack = createStack()
input.map((value) => {
    const [op,  i, addValue] = value.split(" ")

    if(op === "push"){
        console.log(opStack.push(i))
    } else  if(op === "pop"){
        console.log(opStack.pop())
    } else {
        console.log(opStack.add(i, addValue))
    }
})
