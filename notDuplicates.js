const names = ["Pedro", "Mario", "Luisa", "Pedro"]
const prices = [10, 1, 20, 10]
const weights = [10, 10, 10, 10]


//TODO: 1: Mix the array in form: [{name, price, weight}]
//TODO: 2: Count duplicates

//1
const arrayMerge = names.map((item, i) => {
    return item + prices[i] + weights[i]
})
const set = Array.from(new Set(arrayMerge))

return arrayMerge.length - set.length

//2
const result = arrayMerge.reduce((count, value, index) => {
    const { name, price, weight } = value
    const indexPos = arrayMerge.findIndex(item => {
        return item.name === name && item.price === price && item.weight === weight} )
    
        console.log(count)
    if(indexPos !== index)
        return count + 1
    else 
        return count
}, 0)

console.log(result)

